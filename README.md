## Description

This repository contains the source files for the manuscript **"etalon: A simple R toolkit for the evaluation of statistical models"** by Bruneaux and López-Sepulcre.

The [corresponding pdf
files](https://matthieu-bruneaux.gitlab.io/ms-etalon-package/) are
built automatically every time some changes are pushed to the repository.

### How to contribute

You can clone the repository locally from your terminal (requires you to be a
member of the repository):

```
git clone git@gitlab.com:matthieu-bruneaux/ms-etalon-package.git
```

The repository is organised into sub-folders:

 - **current-manuscript/** contains the latest version of the manuscript
   (`manuscript.tex`). This is the file to edit if you want to modify the manuscript.
-  more subfolders will be added to follow submissions and revisions to peer-reviewed journals.

For ease of use, a global `Makefile` is available in the root directory. Type:

- `make all` to generate the latest version of the manuscript.

### Possible peer-reviewed venues

- the R journal (https://journal.r-project.org/)
- the Journal of Statistical Software (https://www.jstatsoft.org/index)
- the Journal of Open Source Software (https://joss.theoj.org/)
- ROpenSci software peer review (https://ropensci.org/software-review/)

