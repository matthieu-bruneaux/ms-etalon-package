%%% * Preamble

\documentclass[a4paper,12pt]{article}

%%% ** Packages

\usepackage[utf8]{inputenc} % Character encoding (for accents)
\usepackage[top=3cm, bottom=3cm, left=3cm, right=2.5cm]{geometry} % Margins
\usepackage{setspace} % Double spacing between text lines
\usepackage{fancyhdr} % For running header
  \pagestyle{fancy}
  \fancyheadoffset{0cm}
\usepackage[authoryear]{natbib} % Citations
\usepackage[colorlinks=true, linkcolor=blue, citecolor=blue, bookmarks=true,
  pdftitle={R package etalon}]{hyperref} % For hyperlinks
\usepackage{soul} % For highlighting
\usepackage{authblk} % For authors' affiliations

%%% ** Custom commands

\newcommand{\etalon}{\texttt{etalon}}

%%% * \begin{document}

%%% ** Title and authors
\title{\etalon{}: A simple R toolkit for the evaluation of statistical models.}
\author[*]{Matthieu Bruneaux}
\author[1]{Andrés López-Sepulcre}
\affil[1]{Department of Biology, Washington University in St. Louis, MO, USA}
\affil[*]{Corresponding author. E-mail: \texttt{matthieu.bruneaux@ens-lyon.org}}

\rhead{\etalon{} package}

\begin{document}

%%% * Title

\maketitle

%%% * Abstract

\clearpage

\lhead{Abstract}
\begin{abstract}

When designing empirical studies, simulations are valuable tools in two ways: (i) they can be used to evaluate the performance of new statistical models and their domains of validity, and (ii) they allow to optimize the design of experimental studies to maximize the chances that observations will be sufficient to answer a given research question.
%
In practive, while power analyses are often recommended as a prerequisite step before running an experiment, the technical burden of handling many simulations and summarizing the data for interpretation and decision making can be a barrier.
%
The \etalon{} package provides a simple toolkit that aims at lowering that barrier by providing a tidy and robust framework to generate parameter values, simulate datasets, run analyses, and compare estimated values to true parameter values.
%
Running simulations requires the researcher to write a data-generating model, which has several added benefits: (i) it encourages them to reflect on the causal mechanisms generating the observations in their study system and (ii) the data-generating model can typically be reused with very little modification as a Bayesian model with a statistical language like JAGS or Stan.
%
By facilitating the handling and analysis of simulations, we hope that \etalon{} will promote a more systematic evaluation of study designs before running experiments or performing observational studies, and will help with the benchmarking of new statistical models.

% MB: Add a word in the last sentence about promoting thinking explicitly about data-generating mechanisms behind a phenomenon?

\end{abstract}

%%% * Introduction

\clearpage
\doublespacing

\lhead{Introduction}
\section{Introduction}

Running simulations before an actual experiment can be important to determine the power of an experimental design as well as to optimize costs, ensuring that the invested time and money will maximize the chances to answer the question. This is good practice, and usually called for, but it is still not common in current research practice (ref to some papers studying this, studying how much actual power analyses are done before running experiments? evaluating how much money is wasted because some experiments are run which in the end could probably never have answered the research question?) \hl{an exception may be the medical field, where they have available 'simulators' to calculate the required N for clinical trials, for example}.

There is an abundant literature about how to evaluate the power of different study designs. Formulas exist for simple cases, but using computer simulations one can explore much more complex situations (Arnold 2011).

Note: I am using the world ``power'' loosely here, not necessarily in the meaning of the ability to detect a true effect using a significance threshold. See table in Morris 2019 for tables showing the variety of performance measures one might want to use (bias, precision, standard error, coverage, type I and type II error, ...).

With the low cost of computing power available today, running simulations to evaluate experimental designs and statistical models is getting easier and very affordable. This necessitates to run some simulations to generate plausible data. This is a very important step, during which researchers are forced to explicit their hypotheses about the real-life data-generating processes behind the observations of their study system, and the simplifications they assume. Also, Bayesian analyses are becoming more common (ref?), and since the use of Bayesian models often requires writing an explicit data generating model by the researcher (e.g. JAGS, Stan) it is trivial to use this model to generate simulated data. The only barrier that might remain for not using this model to perform analyses before choosing an experimental design and running the experiment is the hassle of running and managing many simulations, extracting the estimates of interest and summarizing the data for interpretation and decision making.

The etalon package provides a simple toolkit for use with R \citep{r_core_team_r_2021} that aims at lowering that barrier as much as possible by providing researchers with a robust and tidy framework to generate parameters, simulate datasets, run analyses and compare estimated values to true parameter values. We hope that by providing an easy-to-use tool, researchers will more readily engage in a systematic evaluation of their experimental designs and/or statistical models before running money and time consuming experiments.

Targeted audience:
\begin{enumerate}
\item researchers at the experimental design stage of their project who want to check the feasibility of their analysis and optimize the cost/risk balance of their experiment
\item researchers developing new statistical models/methods who want to assess the performance of their models (bias, precision, conditions of validity/identifiability of the parameters of interest)
\item also useful as a teaching tool? Cf. literature about using simulations as teaching strategy.
\end{enumerate}

% Interesting ref: Morris, T.P., White, I.R., and Crowther, M.J. (2019). Using simulation studies to evaluate statistical methods. Statistics in Medicine 38, 2074–2102.

% Going meta: the experimental design of simulation studies: https://ieeexplore.ieee.org/abstract/document/5429316

Examples (first hits from Google Scholar using "study design ecology evolution simulation"):

- Johnson, P.C.D., Barry, S.J.E., Ferguson, H.M., and Müller, P. (2015). Power analysis for generalized linear mixed models in ecology and evolution. Methods in Ecology and Evolution 6, 133–142.

- Kalinowski, S.T. (2002). Evolutionary and statistical properties of three genetic distances. Molecular Ecology 11, 1263–1273.

- Skarpaas, O., Shea, K., and Bullock, J.M. (2005). Optimizing dispersal study design by Monte Carlo simulation. Journal of Applied Ecology 42, 731–739.


%%% * References

\clearpage
\lhead{References}
\bibliographystyle{my-plainnat}
\bibliography{biblio-curated}

%%% * \end{document}

\end{document}
