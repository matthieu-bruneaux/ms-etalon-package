### * Setup

TOP_DIR = $(shell git rev-parse --show-toplevel)

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** manuscript

## manuscript : build the manuscript pdf (main text only)
.PHONY: manuscript
manuscript:
	@printf "\n"
	@printf "$(GREEN)*** Building the manuscript pdf (main text only) ***$(NC)\n"
	@printf "\n"
	@cd current-manuscript; make pdf
	@cp current-manuscript/manuscript.pdf .

### ** clean

## clean : remove all automatically generated files
.PHONY: clean
clean:
	@printf "\n"
	@printf "$(GREEN)*** Complete cleaning (manuscript and reponse letter) ***$(NC)\n"
	@printf "\n"
	@cd current-manuscript; make clean
	@rm -f manuscript.pdf

## clean-tmp : remove the helper files automatically generated
.PHONY: clean-tmp
clean-tmp:
	@cd current-manuscript; make clean-tmp

### ** all

## all : clean and build pdfs (and remove other output files)
.PHONY: all
all: clean manuscript
